import dayjs from "dayjs";
import axios from "axios";
import { useDispatch } from "react-redux";
import {  doneTodo, removeTodo } from "../../features/todos/todoSlice";
import PropTypes from 'prop-types';
import { url } from "../../../url";


function Todo(props) {
    const todo = props.todo
    const dispatch = useDispatch()
    console.log(todo)

    function handleDelete(todoId) {
        axios.delete(url+'/todos/' + todoId, {withCredentials:true})
        
        .then(()=>{
         dispatch(removeTodo(todoId))
       })
       .catch(err=>console.log(err))
       
    }
    
     function handleDone(todoId,todo){
       const doneValue = todo.done ? true : false;
       axios.patch(url+"/todos/"+todoId,{done: !doneValue},{withCredentials:true})
       .then((res)=>{
         console.log(res)
         dispatch(doneTodo(res.data))
       })
       .catch(err=>console.log(err))
     
     }
  return (
    <li className= { `shadow-lg py-6  flex flex-col justify-center items-center ${todo.done ? "bg-green-400" : ""}`}>
              <div className="flex flex-row justify-between w-full px-2 mb-4">
                <button onClick={()=>handleDone(todo._id,todo)}>
                  <img className="w-6 h-6" src="/icons/done.svg" alt="done" />
                </button>
                <button onClick={()=>handleDelete(todo._id)}>
                  <img className="w-6 h-6" src="/icons/delete.svg" alt="delete" />
                </button>
              </div>
              <h2 className="text-lg font-semibold text-center px-6 mb-14">{todo.title}</h2>
              <div className="flex w-full flex-row justify-between gap-16 px-6">
                <span className="text-sm text-gray-600 ">{dayjs(todo.date).format("MMM DD")}</span>
                <span className="text-sm text-gray-600">{dayjs(todo.date).format("HH:mm A")}</span>
              </div>
            </li>
  )
}

Todo.propTypes = {
    todo: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      done: PropTypes.bool.isRequired,
      title: PropTypes.string.isRequired, 
      date: PropTypes.string.isRequired,
      
    }).isRequired,
  };

export default Todo