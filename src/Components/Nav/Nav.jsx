
import { Link} from "react-router-dom";




function Nav() {

  return (
    <nav className="w-full">
      <ul className="  flex flex-col items-center justify-center md:flex md:flex-row   md:w-full  font-semibold">
        <li className="shadow-lg hover:text-orange-600 w-full p-6 text-center md:shadow-none md:p-0">
          <Link>Home</Link>
        </li>
        <li className="shadow-lg hover:text-orange-600 w-full p-6 text-center md:shadow-none md:p-0">
          <Link to="/todos">My Todos</Link>
        </li>
        <li className="shadow-lg hover:text-orange-600 w-full p-6 text-center md:shadow-none md:p-0">
          <Link to="">Help</Link>
        </li>
      </ul>
      
    </nav>
  )
}

export default Nav