import { createSlice} from '@reduxjs/toolkit'

export const todoSlice = createSlice({
  name: 'todo',
  initialState: {
    todos: []
  },
  reducers: {
    addTodos : (state,action)=>{
        state.todos = action.payload
    },
    addOneTodo : (state,action)=>{
        state.todos.push(action.payload)
    },
    removeTodo: (state,action)=>{
     const todoId = action.payload
     state.todos = state.todos.filter(todo=>todo._id !== todoId)
    },
    doneTodo : (state,action)=>{
      const newTodo = action.payload
    
      const currentTodo = state.todos.find((todo) => todo._id === newTodo._id);
     
      if(currentTodo){
        Object.assign(currentTodo, newTodo)
      }
    },
  }
})

export const {addTodos,addOneTodo,removeTodo,doneTodo} = todoSlice.actions


export default todoSlice.reducer