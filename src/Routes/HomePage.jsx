import { Link } from 'react-router-dom'
function HomePage() {
  return (
    <main className='h-screen  mx-auto my-auto flex flex-row items-center'>
      <section className='hidden md:bg-gradient-to-r md:from-purple-900 md:text-purple-950 md:rounded-br-full md:w-1/3 md:h-full md:flex flex-col justify-center items-center  gap-14'>
        <h2 className='text-4xl font-bold '>Do with Initial Do</h2>
        <h3 className=' pt-8 text-center text-3xl font-bold'>Login to add your todos</h3>
        <Link to="/signup" className='hover:text-orange-600 text-lg font-semibold'>Getting started</Link>
      </section>
      <section className='w-full h-full flex flex-row justify-center items-center'>
        <img  className='h-fit' src="images/todoLogo.png" alt="" />
      </section>
    </main>
  )
}

export default HomePage