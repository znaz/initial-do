import axios from "axios";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, Link} from "react-router-dom";
import {  addOneTodo, addTodos} from "../features/todos/todoSlice";
import Todo from "../Components/Todos/Todo";
import { url } from "../../url";



function TodosPage() {
  const dispatch = useDispatch()
  const todos = useSelector(state=> state.todos.todos)
  const navigate = useNavigate();
  const [todoFormVisible, setTodoFormVisible] = useState(false);

  const handleClick = () => {
    setTodoFormVisible(true);
  };
  async function getData(){
    try{
       await axios.post(url+"/users/verify", {}, { withCredentials: true })
    const todosData = await axios.get(url+"/todos", { withCredentials: true })
    console.log(todosData)
    
    const todos = todosData.data
    return {todos}
      
    }
    catch(err){
      console.log(err)
    }
  }
  useEffect(() => {
    getData().then(data=>{
      
      dispatch(addTodos(data.todos))
    }).catch(err=>{
      console.log(err)
      navigate('/login')
    })
  }, []);


  const handleSubmit = (e) => {
    e.preventDefault();
    const form = e.target;

    const title = form['title'].value;


    axios
      .post(url+"/todos", {title:title}, { withCredentials: true })
      .then((res) => {
        const newTodo = res.data
        console.log(newTodo)
        dispatch(addOneTodo(newTodo))
        setTodoFormVisible(false);
      })
      .catch((err) => {
        console.log(err);
      });
  };


  return (
    <main className="h-full min-h-screen  relative flex flex-col gap-28 mt-20">

      {todoFormVisible && (
        <>
          <div className="fixed top-0 left-0 right-0 bottom-0 h-full w-full bg-black opacity-70">
            &nbsp;
          </div>
          <div className="absolute h-full w-full flex flex-col  items-center mt-12">
            <form
              onSubmit={handleSubmit}
              className="  bg-purple-800  md:w-1/4 h-1/4 rounded-tl-xl  rounded-br-xl "
            >
              <div className="flex flex-col justify-center  items-center gap-3 py-4">
              <Link
                onClick={() => setTodoFormVisible(false)}
                className="self-end mr-3"
              >
                <img className="w-8 h-8 " src="/icons/close.svg" alt="" />
              </Link>

              <label className="text-xl font-semibold mb-3" htmlFor="title">
                Title
              </label>
              <input
                className="border border-purple-800 p-2 mx-6"
                type="text"
                name="title"
                id="title"
              />
              <button
                type="submit"
                className="bg-purple-950 py-1 px-4 font-semibold text-sm mt-4  text-white rounded"
              >
                Add
              </button>
              </div>
            </form>
          </div>
        </>
      )}
      <section className="flex flex-col  justify-center items-center h-2/5">
        <h1 className="text-4xl font-bold text-center text-purple-900">
          My Todos
        </h1>
        <button
          onClick={handleClick}
          className="mt-12 bg-purple-800 text-white font-semibold px-4 py-1 rounded-sm"
        >
          Add Todo
        </button>
      </section>
      <section className="container ml-auto mr-auto">
        <ul className="  grid lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 gap-12  ">

          {
            todos.map((todo)=>{
              
              return <Todo key={todo._id} todo={todo}/>
            })
          }
          
        </ul>
      </section>
    </main>
  );
}

export default TodosPage;
